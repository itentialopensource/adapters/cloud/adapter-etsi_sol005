# ETSI NFV-SOL 005

Vendor: ETSI
Homepage: https://www.etsi.org/

Product: NFV-SOL 005
Product Page: https://www.etsi.org/deliver/etsi_gs/NFV-SOL/001_099/005/03.03.01_60/gs_nfv-sol005v030301p.pdf

## Introduction
We classify ETSI NFV-SOL 005 into the Cloud domain as ETSI NFV-SOL 005 provides capabilities in the management and orchestration of Network Services (NS) and Virtualized Network Functions (VNFs) within cloud environemnts.

## Why Integrate
The ETSI NFV-SOL 005 adapter from Itential is used to integrate the Itential Automation Platform (IAP) with ETSI NFV-SOL 005 to facilitate automation and resource optimization. 

With this adapter you have the ability to perform operations with ETSI NFV-SOL 005 such as:
- Alarms
- Capacity Thresholds
- Subscriptions
- VNF

## Additional Product Documentation
The [API documents for ETSI NFV-SOL 005](https://www.etsi.org/deliver/etsi_gs/NFV-SOL/001_099/005/03.03.01_60/gs_nfv-sol005v030301p.pdf)
