## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for ETSI NFV-SOL 005. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for ETSI NFV-SOL 005.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the ETSI Standard sol005. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getApiVersions(callback)</td>
    <td style="padding:15px">Retrieve API version information</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/api_versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsDescriptors(body, callback)</td>
    <td style="padding:15px">Create a new NS descriptor resource.</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/ns_descriptors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsDescriptors(filter, allFields, fields, excludeFields, excludeDefault, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">Query information about multiple NS descriptor resources.</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/ns_descriptors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsDescriptorsNsdInfoId(nsdInfoId, callback)</td>
    <td style="padding:15px">Read information about an individual NS descriptor resource.</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/ns_descriptors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNsDescriptorsNsdInfoId(nsdInfoId, body, callback)</td>
    <td style="padding:15px">Modify the operational state and/or the user defined data of an individual NS descriptor resource.</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/ns_descriptors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsDescriptorsNsdInfoId(nsdInfoId, callback)</td>
    <td style="padding:15px">Delete an individual NS descriptor resource.</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/ns_descriptors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsDescriptorsNsdInfoIdNsdContent(nsdInfoId, callback)</td>
    <td style="padding:15px">Fetch the content of a NSD.</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/ns_descriptors/{pathv1}/nsd_content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsDescriptorsNsdInfoIdNsdContent(nsdInfoId, callback)</td>
    <td style="padding:15px">Upload the content of a NSD.</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/ns_descriptors/{pathv1}/nsd_content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsDescriptorsNsdInfoIdNsd(nsdInfoId, includeSignatures, callback)</td>
    <td style="padding:15px">The GET method reads the content of the NSD within an NSD archive.
The NSD can be implemented as a</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/ns_descriptors/{pathv1}/nsd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsDescriptorsNsdInfoIdManifest(nsdInfoId, includeSignatures, callback)</td>
    <td style="padding:15px">Fetch the content of the manifest in an NSD archive.</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/ns_descriptors/{pathv1}/manifest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsDescriptorsNsdInfoIdArtifactsArtifactPath(nsdInfoId, artifactPath, includeSignatures, callback)</td>
    <td style="padding:15px">The GET method fetches the content of an individual artifact within
a NSD archive.

This method sha</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/ns_descriptors/{pathv1}/artifacts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPnfDescriptors(body, callback)</td>
    <td style="padding:15px">Create a new PNF descriptor resource.</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/pnf_descriptors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPnfDescriptors(filter, allFields, fields, excludeFields, excludeDefault, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">Query information about multiple PNF descriptor resources.</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/pnf_descriptors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPnfDescriptorsPnfdInfoId(pnfdInfoId, callback)</td>
    <td style="padding:15px">Read an individual PNFD resource.</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/pnf_descriptors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPnfDescriptorsPnfdInfoId(pnfdInfoId, body, callback)</td>
    <td style="padding:15px">Modify the user defined data of an individual PNF descriptor resource.</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/pnf_descriptors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePnfDescriptorsPnfdInfoId(pnfdInfoId, callback)</td>
    <td style="padding:15px">Delete an individual PNF descriptor resource.</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/pnf_descriptors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPnfDescriptorsPnfdInfoIdPnfdContent(pnfdInfoId, callback)</td>
    <td style="padding:15px">Fetch the content of a PNFD.</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/pnf_descriptors/{pathv1}/pnfd_content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPnfDescriptorsPnfdInfoIdPnfdContent(pnfdInfoId, callback)</td>
    <td style="padding:15px">Upload the content of a PNFD.</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/pnf_descriptors/{pathv1}/pnfd_content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPnfDescriptorsPnfdInfoIdPnfd(pnfdInfoId, includeSignatures, callback)</td>
    <td style="padding:15px">The GET method reads the content of the PNFD within a PNFD archive.
The PNFD can be implemented as</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/pnf_descriptors/{pathv1}/pnfd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPnfDescriptorsPnfdInfoIdManifest(pnfdInfoId, includeSignatures, callback)</td>
    <td style="padding:15px">The GET method reads the content of the manifest file within a PNFD archive.
This method shall foll</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/pnf_descriptors/{pathv1}/manifest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPnfDescriptorsPnfdInfoIdArtifactsArtifactPath(pnfdInfoId, artifactPath, includeSignatures, callback)</td>
    <td style="padding:15px">The GET method fetches the content of an individual artifact within a PNFD archive.

This method sh</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/pnf_descriptors/{pathv1}/artifacts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubscriptions(body, callback)</td>
    <td style="padding:15px">Subscribe to NSD and PNFD change notifications.</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/subscriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubscriptions(filter, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">Query multiple subscriptions.</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/subscriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubscriptionsSubscriptionId(subscriptionId, callback)</td>
    <td style="padding:15px">Read an individual subscription resource.</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/subscriptions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubscriptionsSubscriptionId(subscriptionId, callback)</td>
    <td style="padding:15px">Terminate Subscription</td>
    <td style="padding:15px">{base_path}/{version}/nsd/v2/subscriptions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarms(filter, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">Query alarms related to NS instances.</td>
    <td style="padding:15px">{base_path}/{version}/nsfm/v1/alarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmsAlarmId(alarmId, callback)</td>
    <td style="padding:15px">Read individual alarm.</td>
    <td style="padding:15px">{base_path}/{version}/nsfm/v1/alarms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAlarmsAlarmId(alarmId, body, callback)</td>
    <td style="padding:15px">Acknowledge individual alarm.</td>
    <td style="padding:15px">{base_path}/{version}/nsfm/v1/alarms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfPackages(filter, allFields, fields, excludeFields, excludeDefault, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">Query VNF packages information.</td>
    <td style="padding:15px">{base_path}/{version}/vnfpkgm/v2/vnf_packages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfPackages(body, callback)</td>
    <td style="padding:15px">Create a new individual VNF package resource.</td>
    <td style="padding:15px">{base_path}/{version}/vnfpkgm/v2/vnf_packages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfPackagesVnfPkgId(vnfPkgId, callback)</td>
    <td style="padding:15px">Read information about an individual VNF package.</td>
    <td style="padding:15px">{base_path}/{version}/vnfpkgm/v2/vnf_packages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVnfPackagesVnfPkgId(vnfPkgId, callback)</td>
    <td style="padding:15px">Delete an individual VNF package.</td>
    <td style="padding:15px">{base_path}/{version}/vnfpkgm/v2/vnf_packages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVnfPackagesVnfPkgId(vnfPkgId, body, callback)</td>
    <td style="padding:15px">Update information about an individual VNF package.</td>
    <td style="padding:15px">{base_path}/{version}/vnfpkgm/v2/vnf_packages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfPackagesVnfPkgIdVnfd(vnfPkgId, includeSignatures, callback)</td>
    <td style="padding:15px">Read VNFD of an on-boarded VNF package.</td>
    <td style="padding:15px">{base_path}/{version}/vnfpkgm/v2/vnf_packages/{pathv1}/vnfd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfPackagesVnfPkgIdExtArtifactsAccess(vnfPkgId, callback)</td>
    <td style="padding:15px">Get the content of external VNF package artifacts.</td>
    <td style="padding:15px">{base_path}/{version}/vnfpkgm/v2/vnf_packages/{pathv1}/ext_artifacts_access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVnfPackagesVnfPkgIdExtArtifactsAccess(vnfPkgId, body, callback)</td>
    <td style="padding:15px">Download the content of external VNF package artifacts.</td>
    <td style="padding:15px">{base_path}/{version}/vnfpkgm/v2/vnf_packages/{pathv1}/ext_artifacts_access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfPackagesVnfPkgIdManifest(vnfPkgId, includeSignatures, callback)</td>
    <td style="padding:15px">The GET method reads the content of the manifest within a VNF package.
</td>
    <td style="padding:15px">{base_path}/{version}/vnfpkgm/v2/vnf_packages/{pathv1}/manifest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfPackagesVnfPkgIdPackageContent(vnfPkgId, callback)</td>
    <td style="padding:15px">Fetch an on-boarded VNF package.</td>
    <td style="padding:15px">{base_path}/{version}/vnfpkgm/v2/vnf_packages/{pathv1}/package_content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVnfPackagesVnfPkgIdPackageContent(vnfPkgId, body, callback)</td>
    <td style="padding:15px">Upload a VNF package by providing the content of the VNF package.</td>
    <td style="padding:15px">{base_path}/{version}/vnfpkgm/v2/vnf_packages/{pathv1}/package_content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfPackagesVnfPkgIdArtifacts(vnfPkgId, includeSignatures, excludeAllManoArtifacts, excludeAllNonManoArtifacts, selectNonManoArtifactSets, includeExternalArtifacts, callback)</td>
    <td style="padding:15px">Fetch set of VNF package artifacts.</td>
    <td style="padding:15px">{base_path}/{version}/vnfpkgm/v2/vnf_packages/{pathv1}/artifacts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfPackagesVnfPkgIdPackageContentUploadFromUri(vnfPkgId, body, callback)</td>
    <td style="padding:15px">Upload a VNF package by providing the address information of the VNF package.</td>
    <td style="padding:15px">{base_path}/{version}/vnfpkgm/v2/vnf_packages/{pathv1}/package_content/upload_from_uri?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfPackagesVnfPkgIdArtifactsArtifactPath(vnfPkgId, artifactPath, includeSignatures, callback)</td>
    <td style="padding:15px">Fetch individual VNF package artifact.</td>
    <td style="padding:15px">{base_path}/{version}/vnfpkgm/v2/vnf_packages/{pathv1}/artifacts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPmJobs(body, callback)</td>
    <td style="padding:15px">Create a PM job.</td>
    <td style="padding:15px">{base_path}/{version}/nspm/v2/pm_jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPmJobs(filter, allFields, fields, excludeFields, excludeDefault, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">Query PM jobs.</td>
    <td style="padding:15px">{base_path}/{version}/nspm/v2/pm_jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPmJobsPmJobId(pmJobId, callback)</td>
    <td style="padding:15px">Read a single PM job.</td>
    <td style="padding:15px">{base_path}/{version}/nspm/v2/pm_jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPmJobsPmJobId(pmJobId, body, callback)</td>
    <td style="padding:15px">Modify a PM job.</td>
    <td style="padding:15px">{base_path}/{version}/nspm/v2/pm_jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePmJobsPmJobId(pmJobId, callback)</td>
    <td style="padding:15px">Delete a PM job.</td>
    <td style="padding:15px">{base_path}/{version}/nspm/v2/pm_jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPmJobsPmJobIdReportsReportId(pmJobId, reportId, callback)</td>
    <td style="padding:15px">Read an individual performance report.</td>
    <td style="padding:15px">{base_path}/{version}/nspm/v2/pm_jobs/{pathv1}/reports/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postThresholds(body, callback)</td>
    <td style="padding:15px">Create a threshold.</td>
    <td style="padding:15px">{base_path}/{version}/nspm/v2/thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThresholds(filter, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">Query thresholds.</td>
    <td style="padding:15px">{base_path}/{version}/nspm/v2/thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThresholdsThresholdId(thresholdId, callback)</td>
    <td style="padding:15px">Query a single threshold.</td>
    <td style="padding:15px">{base_path}/{version}/nspm/v2/thresholds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchThresholdsThresholdId(thresholdId, body, callback)</td>
    <td style="padding:15px">Modify a Threshold</td>
    <td style="padding:15px">{base_path}/{version}/nspm/v2/thresholds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteThresholdsThresholdId(thresholdId, callback)</td>
    <td style="padding:15px">Delete a Threshold.</td>
    <td style="padding:15px">{base_path}/{version}/nspm/v2/thresholds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNfviCapacityInfos(filter, allFields, fields, excludeFields, excludeDefault, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">Query NFVI capacity information</td>
    <td style="padding:15px">{base_path}/{version}/nfvici/v1/nfvi_capacity_infos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNfviCapacityInfosVimId(vimId, filter, callback)</td>
    <td style="padding:15px">Query NFVI capacity information for a specific VIM</td>
    <td style="padding:15px">{base_path}/{version}/nfvici/v1/nfvi_capacity_infos/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCapacityThresholds(filter, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">Query NFVI capacity thresholds</td>
    <td style="padding:15px">{base_path}/{version}/nfvici/v1/capacity_thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCapacityThresholds(body, callback)</td>
    <td style="padding:15px">Create a NFVI capacity threshold</td>
    <td style="padding:15px">{base_path}/{version}/nfvici/v1/capacity_thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCapacityThresholdsCapacityThresholdId(capacityThresholdId, callback)</td>
    <td style="padding:15px">Query Individual NFVI capacity threshold</td>
    <td style="padding:15px">{base_path}/{version}/nfvici/v1/capacity_thresholds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCapacityThresholdsCapacityThresholdId(capacityThresholdId, body, callback)</td>
    <td style="padding:15px">Modify an "Individual capacity threshold" resource</td>
    <td style="padding:15px">{base_path}/{version}/nfvici/v1/capacity_thresholds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCapacityThresholdsCapacityThresholdId(capacityThresholdId, callback)</td>
    <td style="padding:15px">Delete an NFVI capacity threshold</td>
    <td style="padding:15px">{base_path}/{version}/nfvici/v1/capacity_thresholds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfSnapshotPackages(body, callback)</td>
    <td style="padding:15px">The POST method creates a new "Individual VNF snapshot package" resource.
</td>
    <td style="padding:15px">{base_path}/{version}/vnfsnapshotpkgm/v1/vnf_snapshot_packages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfSnapshotPackages(filter, allFields, fields, excludeFields, excludeDefault, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">The GET method queries the information of the VNF packages matching the filter.
</td>
    <td style="padding:15px">{base_path}/{version}/vnfsnapshotpkgm/v1/vnf_snapshot_packages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfSnapshotPackagesVnfSnapshotPkgId(vnfSnapshotPkgId, callback)</td>
    <td style="padding:15px">The GET method reads the information of an individual VNF snapshot package.
</td>
    <td style="padding:15px">{base_path}/{version}/vnfsnapshotpkgm/v1/vnf_snapshot_packages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVnfSnapshotPackagesVnfSnapshotPkgId(vnfSnapshotPkgId, body, callback)</td>
    <td style="padding:15px">The PATCH method updates the information of a VNF snapshot package.
</td>
    <td style="padding:15px">{base_path}/{version}/vnfsnapshotpkgm/v1/vnf_snapshot_packages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVnfSnapshotPackagesVnfSnapshotPkgId(vnfSnapshotPkgId, callback)</td>
    <td style="padding:15px">The DELETE method deletes an "Individual VNF snapshot package" resource.
</td>
    <td style="padding:15px">{base_path}/{version}/vnfsnapshotpkgm/v1/vnf_snapshot_packages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfSnapshotPackagesVnfSnapshotPkgIdPackageContent(vnfSnapshotPkgId, callback)</td>
    <td style="padding:15px">The GET method fetches the content of a VNF snapshot package.

The content of the package is provid</td>
    <td style="padding:15px">{base_path}/{version}/vnfsnapshotpkgm/v1/vnf_snapshot_packages/{pathv1}/package_content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVnfSnapshotPackagesVnfSnapshotPkgIdPackageContent(vnfSnapshotPkgId, callback)</td>
    <td style="padding:15px">The PUT method uploads the content of a VNF package.

The payload body contains a ZIP file that rep</td>
    <td style="padding:15px">{base_path}/{version}/vnfsnapshotpkgm/v1/vnf_snapshot_packages/{pathv1}/package_content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentUploadFromUri(vnfSnapshotPkgId, body, callback)</td>
    <td style="padding:15px">The POST method provides the information for the NFVO to get the content of 
a VNF snapshot package</td>
    <td style="padding:15px">{base_path}/{version}/vnfsnapshotpkgm/v1/vnf_snapshot_packages/{pathv1}/package_content/upload_from_uri?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentBuild(vnfSnapshotPkgId, body, callback)</td>
    <td style="padding:15px">The POST method provides the information for the NFVO to start building 
the content of a VNF snaps</td>
    <td style="padding:15px">{base_path}/{version}/vnfsnapshotpkgm/v1/vnf_snapshot_packages/{pathv1}/package_content/build?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentExtract(vnfSnapshotPkgId, body, callback)</td>
    <td style="padding:15px">The POST method provides the information for the NFVO to start extracting the content of 
a VNF sna</td>
    <td style="padding:15px">{base_path}/{version}/vnfsnapshotpkgm/v1/vnf_snapshot_packages/{pathv1}/package_content/extract?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentCancel(vnfSnapshotPkgId, body, callback)</td>
    <td style="padding:15px">The POST method provides the information for the NFVO to cancel the ongoing operation related to 
t</td>
    <td style="padding:15px">{base_path}/{version}/vnfsnapshotpkgm/v1/vnf_snapshot_packages/{pathv1}/package_content/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfSnapshotPackagesVnfSnapshotPkgIdExtArtifactsAccess(vnfSnapshotPkgId, callback)</td>
    <td style="padding:15px">The GET method reads the access configuration information that is used by the NFVO to get 
the cont</td>
    <td style="padding:15px">{base_path}/{version}/vnfsnapshotpkgm/v1/vnf_snapshot_packages/{pathv1}/ext_artifacts_access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVnfSnapshotPackagesVnfSnapshotPkgIdExtArtifactsAccess(vnfSnapshotPkgId, body, callback)</td>
    <td style="padding:15px">The PUT method provides the access configuration information for the NFVO to download 
the content</td>
    <td style="padding:15px">{base_path}/{version}/vnfsnapshotpkgm/v1/vnf_snapshot_packages/{pathv1}/ext_artifacts_access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfSnapshotPackagesVnfSnapshotPkgIdArtifactsArtifactPath(vnfSnapshotPkgId, artifactPath, callback)</td>
    <td style="padding:15px">The GET method fetches the content of an artifact within the VNF snapshot package.

If the VNF snap</td>
    <td style="padding:15px">{base_path}/{version}/vnfsnapshotpkgm/v1/vnf_snapshot_packages/{pathv1}/artifacts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">querymultipleNSinstances(filter, allFields, fields, excludeFields, excludeDefault, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">Query multiple NS instances.</td>
    <td style="padding:15px">{base_path}/{version}/nslcm/v1/ns_instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createaNSinstanceresource(body, callback)</td>
    <td style="padding:15px">Create a NS instance resource.</td>
    <td style="padding:15px">{base_path}/{version}/nslcm/v1/ns_instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readanindividualNSinstanceresource(nsInstanceId, callback)</td>
    <td style="padding:15px">Read an individual NS instance resource.</td>
    <td style="padding:15px">{base_path}/{version}/nslcm/v1/ns_instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNSinstanceresource(nsInstanceId, callback)</td>
    <td style="padding:15px">Delete NS instance resource.</td>
    <td style="padding:15px">{base_path}/{version}/nslcm/v1/ns_instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">instantiateaNS(nsInstanceId, body, callback)</td>
    <td style="padding:15px">Instantiate a NS.</td>
    <td style="padding:15px">{base_path}/{version}/nslcm/v1/ns_instances/{pathv1}/instantiate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scaleaNSinstance(nsInstanceId, body, callback)</td>
    <td style="padding:15px">Scale a NS instance.</td>
    <td style="padding:15px">{base_path}/{version}/nslcm/v1/ns_instances/{pathv1}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatesaNSinstance(nsInstanceId, body, callback)</td>
    <td style="padding:15px">Updates a NS instance.</td>
    <td style="padding:15px">{base_path}/{version}/nslcm/v1/ns_instances/{pathv1}/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">healaNSinstance(nsInstanceId, body, callback)</td>
    <td style="padding:15px">Heal a NS instance.</td>
    <td style="padding:15px">{base_path}/{version}/nslcm/v1/ns_instances/{pathv1}/heal?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">terminateaNSinstance(nsInstanceId, body, callback)</td>
    <td style="padding:15px">Terminate a NS instance.</td>
    <td style="padding:15px">{base_path}/{version}/nslcm/v1/ns_instances/{pathv1}/terminate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">querymultipleNSLCMoperationoccurrences(filter, fields, excludeFields, excludeDefault, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">Query multiple NS LCM operation occurrences.</td>
    <td style="padding:15px">{base_path}/{version}/nslcm/v1/ns_lcm_op_occs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readanindividualNSLCMoperationoccurrenceresource(nsLcmOpOccId, callback)</td>
    <td style="padding:15px">Read an individual NS LCM operation occurrence resource.</td>
    <td style="padding:15px">{base_path}/{version}/nslcm/v1/ns_lcm_op_occs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retryaNSlifecyclemanagementoperationoccurrence(nsLcmOpOccId, callback)</td>
    <td style="padding:15px">Retry a NS lifecycle management operation occurrence.</td>
    <td style="padding:15px">{base_path}/{version}/nslcm/v1/ns_lcm_op_occs/{pathv1}/retry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rollbackaNSlifecyclemanagementoperationoccurrence(nsLcmOpOccId, callback)</td>
    <td style="padding:15px">Rollback a NS lifecycle management operation occurrence.</td>
    <td style="padding:15px">{base_path}/{version}/nslcm/v1/ns_lcm_op_occs/{pathv1}/rollback?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">continueaNSlifecyclemanagementoperationoccurrence(nsLcmOpOccId, callback)</td>
    <td style="padding:15px">Continue a NS lifecycle management operation occurrence.</td>
    <td style="padding:15px">{base_path}/{version}/nslcm/v1/ns_lcm_op_occs/{pathv1}/continue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">markaNSlifecyclemanagementoperationoccurrenceasfailed(nsLcmOpOccId, callback)</td>
    <td style="padding:15px">Mark a NS lifecycle management operation occurrence as failed.</td>
    <td style="padding:15px">{base_path}/{version}/nslcm/v1/ns_lcm_op_occs/{pathv1}/fail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelaNSlifecyclemanagementoperationoccurrence(nsLcmOpOccId, body, callback)</td>
    <td style="padding:15px">Cancel a NS lifecycle management operation occurrence.</td>
    <td style="padding:15px">{base_path}/{version}/nslcm/v1/ns_lcm_op_occs/{pathv1}/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryVNFsnapshots(filter, allFields, fields, excludeFields, excludeDefault, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">Query VNF snapshots</td>
    <td style="padding:15px">{base_path}/{version}/nslcm/v1/vnf_snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryanIndividualVNFsnapshot(vnfSnapshotInfoId, callback)</td>
    <td style="padding:15px">Query an Individual VNF snapshot</td>
    <td style="padding:15px">{base_path}/{version}/nslcm/v1/vnf_snapshots/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteanIndividualVNFsnapshot(vnfSnapshotInfoId, callback)</td>
    <td style="padding:15px">Delete an Individual VNF snapshot</td>
    <td style="padding:15px">{base_path}/{version}/nslcm/v1/vnf_snapshots/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
