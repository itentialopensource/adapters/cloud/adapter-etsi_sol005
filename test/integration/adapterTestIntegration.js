/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-etsi_sol005',
      type: 'EtsiSol005',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const EtsiSol005 = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Etsi_sol005 Adapter Test', () => {
  describe('EtsiSol005 Class Tests', () => {
    const a = new EtsiSol005(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */
    describe('#getApiVersions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApiVersions((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.uriPrefix);
                assert.equal(true, Array.isArray(data.response.apiVersions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiVersions', 'getApiVersions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nsDescriptorsPostNsDescriptorsBodyParam = {
      userDefinedData: {}
    };
    describe('#postNsDescriptors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postNsDescriptors(nsDescriptorsPostNsDescriptorsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.nsdId);
                assert.equal('string', data.response.nsdName);
                assert.equal('string', data.response.nsdVersion);
                assert.equal('string', data.response.nsdDesigner);
                assert.equal('string', data.response.nsdInvariantId);
                assert.equal(true, Array.isArray(data.response.vnfPkgIds));
                assert.equal(true, Array.isArray(data.response.pnfdInfoIds));
                assert.equal(true, Array.isArray(data.response.nestedNsdInfoIds));
                assert.equal('OPTION_2', data.response.archiveSecurityOption);
                assert.equal('string', data.response.signingCertificate);
                assert.equal(true, Array.isArray(data.response.artifacts));
                assert.equal('ONBOARDED', data.response.nsdOnboardingState);
                assert.equal('object', typeof data.response.onboardingFailureDetails);
                assert.equal('DISABLED', data.response.nsdOperationalState);
                assert.equal('NOT_IN_USE', data.response.nsdUsageState);
                assert.equal('object', typeof data.response.userDefinedData);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsDescriptors', 'postNsDescriptors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsDescriptors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNsDescriptors(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsDescriptors', 'getNsDescriptors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nsDescriptorsNsdInfoId = 'fakedata';
    const nsDescriptorsPatchNsDescriptorsNsdInfoIdBodyParam = {
      nsdOperationalState: 'ENABLED',
      userDefinedData: [
        {}
      ]
    };
    describe('#patchNsDescriptorsNsdInfoId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchNsDescriptorsNsdInfoId(nsDescriptorsNsdInfoId, nsDescriptorsPatchNsDescriptorsNsdInfoIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsDescriptors', 'patchNsDescriptorsNsdInfoId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsDescriptorsNsdInfoId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNsDescriptorsNsdInfoId(nsDescriptorsNsdInfoId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.nsdId);
                assert.equal('string', data.response.nsdName);
                assert.equal('string', data.response.nsdVersion);
                assert.equal('string', data.response.nsdDesigner);
                assert.equal('string', data.response.nsdInvariantId);
                assert.equal(true, Array.isArray(data.response.vnfPkgIds));
                assert.equal(true, Array.isArray(data.response.pnfdInfoIds));
                assert.equal(true, Array.isArray(data.response.nestedNsdInfoIds));
                assert.equal('OPTION_2', data.response.archiveSecurityOption);
                assert.equal('string', data.response.signingCertificate);
                assert.equal(true, Array.isArray(data.response.artifacts));
                assert.equal('CREATED', data.response.nsdOnboardingState);
                assert.equal('object', typeof data.response.onboardingFailureDetails);
                assert.equal('DISABLED', data.response.nsdOperationalState);
                assert.equal('IN_USE', data.response.nsdUsageState);
                assert.equal('object', typeof data.response.userDefinedData);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsDescriptors', 'getNsDescriptorsNsdInfoId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nsDescriptorsArtifactPath = 'fakedata';
    describe('#getNsDescriptorsNsdInfoIdArtifactsArtifactPath - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsDescriptorsNsdInfoIdArtifactsArtifactPath(nsDescriptorsNsdInfoId, nsDescriptorsArtifactPath, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsDescriptors', 'getNsDescriptorsNsdInfoIdArtifactsArtifactPath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsDescriptorsNsdInfoIdManifest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsDescriptorsNsdInfoIdManifest(nsDescriptorsNsdInfoId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsDescriptors', 'getNsDescriptorsNsdInfoIdManifest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsDescriptorsNsdInfoIdNsd - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsDescriptorsNsdInfoIdNsd(nsDescriptorsNsdInfoId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsDescriptors', 'getNsDescriptorsNsdInfoIdNsd', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNsDescriptorsNsdInfoIdNsdContent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putNsDescriptorsNsdInfoIdNsdContent(nsDescriptorsNsdInfoId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsDescriptors', 'putNsDescriptorsNsdInfoIdNsdContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsDescriptorsNsdInfoIdNsdContent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsDescriptorsNsdInfoIdNsdContent(nsDescriptorsNsdInfoId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsDescriptors', 'getNsDescriptorsNsdInfoIdNsdContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pnfDescriptorsPostPnfDescriptorsBodyParam = {
      userDefinedData: {}
    };
    describe('#postPnfDescriptors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postPnfDescriptors(pnfDescriptorsPostPnfDescriptorsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.pnfdId);
                assert.equal('string', data.response.pnfdName);
                assert.equal('string', data.response.pnfdersion);
                assert.equal('string', data.response.pnfdProvider);
                assert.equal('string', data.response.pnfdInvariantId);
                assert.equal('OPTION_1', data.response.archiveSecurityOption);
                assert.equal('string', data.response.signingCertificate);
                assert.equal(true, Array.isArray(data.response.artifacts));
                assert.equal('ERROR', data.response.pnfdOnboardingState);
                assert.equal('object', typeof data.response.onboardingFailureDetails);
                assert.equal('IN_USE', data.response.pnfdUsageState);
                assert.equal('object', typeof data.response.userDefinedData);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PnfDescriptors', 'postPnfDescriptors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPnfDescriptors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPnfDescriptors(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PnfDescriptors', 'getPnfDescriptors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pnfDescriptorsPnfdInfoId = 'fakedata';
    const pnfDescriptorsPatchPnfDescriptorsPnfdInfoIdBodyParam = {
      userDefinedData: {}
    };
    describe('#patchPnfDescriptorsPnfdInfoId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchPnfDescriptorsPnfdInfoId(pnfDescriptorsPnfdInfoId, pnfDescriptorsPatchPnfDescriptorsPnfdInfoIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PnfDescriptors', 'patchPnfDescriptorsPnfdInfoId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPnfDescriptorsPnfdInfoId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPnfDescriptorsPnfdInfoId(pnfDescriptorsPnfdInfoId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.pnfdId);
                assert.equal('string', data.response.pnfdName);
                assert.equal('string', data.response.pnfdersion);
                assert.equal('string', data.response.pnfdProvider);
                assert.equal('string', data.response.pnfdInvariantId);
                assert.equal('OPTION_2', data.response.archiveSecurityOption);
                assert.equal('string', data.response.signingCertificate);
                assert.equal(true, Array.isArray(data.response.artifacts));
                assert.equal('ERROR', data.response.pnfdOnboardingState);
                assert.equal('object', typeof data.response.onboardingFailureDetails);
                assert.equal('NOT_IN_USE', data.response.pnfdUsageState);
                assert.equal('object', typeof data.response.userDefinedData);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PnfDescriptors', 'getPnfDescriptorsPnfdInfoId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pnfDescriptorsArtifactPath = 'fakedata';
    describe('#getPnfDescriptorsPnfdInfoIdArtifactsArtifactPath - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPnfDescriptorsPnfdInfoIdArtifactsArtifactPath(pnfDescriptorsPnfdInfoId, pnfDescriptorsArtifactPath, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PnfDescriptors', 'getPnfDescriptorsPnfdInfoIdArtifactsArtifactPath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPnfDescriptorsPnfdInfoIdManifest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPnfDescriptorsPnfdInfoIdManifest(pnfDescriptorsPnfdInfoId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PnfDescriptors', 'getPnfDescriptorsPnfdInfoIdManifest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPnfDescriptorsPnfdInfoIdPnfd - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPnfDescriptorsPnfdInfoIdPnfd(pnfDescriptorsPnfdInfoId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PnfDescriptors', 'getPnfDescriptorsPnfdInfoIdPnfd', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPnfDescriptorsPnfdInfoIdPnfdContent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putPnfDescriptorsPnfdInfoIdPnfdContent(pnfDescriptorsPnfdInfoId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PnfDescriptors', 'putPnfDescriptorsPnfdInfoIdPnfdContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPnfDescriptorsPnfdInfoIdPnfdContent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPnfDescriptorsPnfdInfoIdPnfdContent(pnfDescriptorsPnfdInfoId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PnfDescriptors', 'getPnfDescriptorsPnfdInfoIdPnfdContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let subscriptionsSubscriptionId = 'fakedata';
    const subscriptionsPostSubscriptionsBodyParam = {
      callbackUri: 'string'
    };
    describe('#postSubscriptions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSubscriptions(subscriptionsPostSubscriptionsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.filter);
                assert.equal('string', data.response.callbackUri);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              subscriptionsSubscriptionId = data.response.id;
              saveMockData('Subscriptions', 'postSubscriptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSubscriptions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSubscriptions(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'getSubscriptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSubscriptionsSubscriptionId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSubscriptionsSubscriptionId(subscriptionsSubscriptionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.filter);
                assert.equal('string', data.response.callbackUri);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'getSubscriptionsSubscriptionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNsDescriptorsNsdInfoId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNsDescriptorsNsdInfoId(nsDescriptorsNsdInfoId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsDescriptors', 'deleteNsDescriptorsNsdInfoId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePnfDescriptorsPnfdInfoId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePnfDescriptorsPnfdInfoId(pnfDescriptorsPnfdInfoId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PnfDescriptors', 'deletePnfDescriptorsPnfdInfoId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSubscriptionsSubscriptionId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSubscriptionsSubscriptionId(subscriptionsSubscriptionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriptions', 'deleteSubscriptionsSubscriptionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlarms(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmsAlarmId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlarmsAlarmId('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.managedObjectId);
                assert.equal('object', typeof data.response.rootCauseFaultyComponent);
                assert.equal('object', typeof data.response.rootCauseFaultyResource);
                assert.equal('ACKNOWLEDGED', data.response.ackState);
                assert.equal('CLEARED', data.response.perceivedSeverity);
                assert.equal('EQUIPMENT_ALARM', data.response.eventType);
                assert.equal('string', data.response.faultType);
                assert.equal('string', data.response.probableCause);
                assert.equal(false, data.response.isRootCause);
                assert.equal(true, Array.isArray(data.response.correlatedAlarmIds));
                assert.equal('string', data.response.faultDetails);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getAlarmsAlarmId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmsPatchAlarmsAlarmIdBodyParam = {
      ackState: 'ACKNOWLEDGED'
    };
    describe('#patchAlarmsAlarmId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchAlarmsAlarmId('fakedata', alarmsPatchAlarmsAlarmIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'patchAlarmsAlarmId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVnfPackages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVnfPackages(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfPackages', 'getVnfPackages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vnfPackagesPostVnfPackagesBodyParam = {
      userDefinedData: {}
    };
    describe('#postVnfPackages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postVnfPackages(vnfPackagesPostVnfPackagesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.vnfdId);
                assert.equal('string', data.response.vnfProvider);
                assert.equal('string', data.response.vnfProductName);
                assert.equal('string', data.response.vnfSoftwareVersion);
                assert.equal('string', data.response.vnfdVersion);
                assert.equal('string', data.response.compatibleSpecificationVersions);
                assert.equal('object', typeof data.response.checksum);
                assert.equal('OPTION_1', data.response.packageSecurityOption);
                assert.equal('string', data.response.signingCertificate);
                assert.equal(true, Array.isArray(data.response.softwareImages));
                assert.equal(true, Array.isArray(data.response.additionalArtifacts));
                assert.equal('UPLOADING', data.response.onboardingState);
                assert.equal('ENABLED', data.response.operationalState);
                assert.equal('IN_USE', data.response.usageState);
                assert.equal(true, Array.isArray(data.response.vnfmInfo));
                assert.equal('object', typeof data.response.userDefinedData);
                assert.equal('object', typeof data.response.onboardingFailureDetails);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfPackages', 'postVnfPackages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVnfPackagesVnfPkgId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVnfPackagesVnfPkgId('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.vnfdId);
                assert.equal('string', data.response.vnfProvider);
                assert.equal('string', data.response.vnfProductName);
                assert.equal('string', data.response.vnfSoftwareVersion);
                assert.equal('string', data.response.vnfdVersion);
                assert.equal('string', data.response.compatibleSpecificationVersions);
                assert.equal('object', typeof data.response.checksum);
                assert.equal('OPTION_2', data.response.packageSecurityOption);
                assert.equal('string', data.response.signingCertificate);
                assert.equal(true, Array.isArray(data.response.softwareImages));
                assert.equal(true, Array.isArray(data.response.additionalArtifacts));
                assert.equal('ONBOARDED', data.response.onboardingState);
                assert.equal('ENABLED', data.response.operationalState);
                assert.equal('NOT_IN_USE', data.response.usageState);
                assert.equal(true, Array.isArray(data.response.vnfmInfo));
                assert.equal('object', typeof data.response.userDefinedData);
                assert.equal('object', typeof data.response.onboardingFailureDetails);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfPackages', 'getVnfPackagesVnfPkgId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVnfPackagesVnfPkgId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVnfPackagesVnfPkgId('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfPackages', 'deleteVnfPackagesVnfPkgId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vnfPackagesPatchVnfPackagesVnfPkgIdBodyParam = {
      operationalState: 'DISABLED',
      userDefinedData: {}
    };
    describe('#patchVnfPackagesVnfPkgId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchVnfPackagesVnfPkgId('fakedata', vnfPackagesPatchVnfPackagesVnfPkgIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfPackages', 'patchVnfPackagesVnfPkgId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVnfPackagesVnfPkgIdVnfd - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVnfPackagesVnfPkgIdVnfd('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfPackages', 'getVnfPackagesVnfPkgIdVnfd', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVnfPackagesVnfPkgIdExtArtifactsAccess - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVnfPackagesVnfPkgIdExtArtifactsAccess('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.artifact);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfPackages', 'getVnfPackagesVnfPkgIdExtArtifactsAccess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vnfPackagesPutVnfPackagesVnfPkgIdExtArtifactsAccessBodyParam = {
      artifact: {
        artifactUri: 'string',
        overrideUri: 'string',
        authType: 'OAUTH2_CLIENT_CREDENTIALS',
        username: 'string',
        password: 'string',
        paramsOauth2ClientCredentials: {
          clientId: 'string',
          clientPassword: 'string',
          tokenEndpoint: 'string'
        }
      }
    };
    describe('#putVnfPackagesVnfPkgIdExtArtifactsAccess - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putVnfPackagesVnfPkgIdExtArtifactsAccess('fakedata', vnfPackagesPutVnfPackagesVnfPkgIdExtArtifactsAccessBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfPackages', 'putVnfPackagesVnfPkgIdExtArtifactsAccess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVnfPackagesVnfPkgIdManifest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVnfPackagesVnfPkgIdManifest('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfPackages', 'getVnfPackagesVnfPkgIdManifest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVnfPackagesVnfPkgIdPackageContent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVnfPackagesVnfPkgIdPackageContent('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfPackages', 'getVnfPackagesVnfPkgIdPackageContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vnfPackagesPutVnfPackagesVnfPkgIdPackageContentBodyParam = {
      file: 'string'
    };
    describe('#putVnfPackagesVnfPkgIdPackageContent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putVnfPackagesVnfPkgIdPackageContent('fakedata', vnfPackagesPutVnfPackagesVnfPkgIdPackageContentBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfPackages', 'putVnfPackagesVnfPkgIdPackageContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVnfPackagesVnfPkgIdArtifacts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVnfPackagesVnfPkgIdArtifacts('fakedata', null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfPackages', 'getVnfPackagesVnfPkgIdArtifacts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vnfPackagesPostVnfPackagesVnfPkgIdPackageContentUploadFromUriBodyParam = {
      addressInformation: 'string'
    };
    describe('#postVnfPackagesVnfPkgIdPackageContentUploadFromUri - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postVnfPackagesVnfPkgIdPackageContentUploadFromUri('fakedata', vnfPackagesPostVnfPackagesVnfPkgIdPackageContentUploadFromUriBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfPackages', 'postVnfPackagesVnfPkgIdPackageContentUploadFromUri', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVnfPackagesVnfPkgIdArtifactsArtifactPath - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVnfPackagesVnfPkgIdArtifactsArtifactPath('fakedata', 'fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfPackages', 'getVnfPackagesVnfPkgIdArtifactsArtifactPath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pmJobsPostPmJobsBodyParam = {
      objectType: 'string',
      objectInstanceIds: [
        'string'
      ],
      criteria: {},
      callbackUri: 'string'
    };
    describe('#postPmJobs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postPmJobs(pmJobsPostPmJobsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.objectType);
                assert.equal(true, Array.isArray(data.response.objectInstanceIds));
                assert.equal(true, Array.isArray(data.response.subObjectInstanceIds));
                assert.equal('object', typeof data.response.criteria);
                assert.equal('string', data.response.callbackUri);
                assert.equal('object', typeof data.response.reports);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PmJobs', 'postPmJobs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPmJobs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPmJobs(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PmJobs', 'getPmJobs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPmJobsPmJobId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPmJobsPmJobId('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.objectType);
                assert.equal(true, Array.isArray(data.response.objectInstanceIds));
                assert.equal(true, Array.isArray(data.response.subObjectInstanceIds));
                assert.equal('object', typeof data.response.criteria);
                assert.equal('string', data.response.callbackUri);
                assert.equal('object', typeof data.response.reports);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PmJobs', 'getPmJobsPmJobId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pmJobsPatchPmJobsPmJobIdBodyParam = {
      callbackUri: 'string',
      authentication: {
        authType: [
          'BASIC'
        ],
        paramsBasic: {
          userName: 'string',
          password: 'string'
        },
        paramsOauth2ClientCredentials: {
          clientId: 'string',
          clientPassword: 'string',
          tokenEndpoint: 'string'
        }
      }
    };
    describe('#patchPmJobsPmJobId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchPmJobsPmJobId('fakedata', pmJobsPatchPmJobsPmJobIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PmJobs', 'patchPmJobsPmJobId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePmJobsPmJobId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePmJobsPmJobId('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PmJobs', 'deletePmJobsPmJobId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPmJobsPmJobIdReportsReportId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPmJobsPmJobIdReportsReportId('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.entries));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PmJobs', 'getPmJobsPmJobIdReportsReportId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const thresholdsPostThresholdsBodyParam = {
      objectType: 'string',
      objectInstanceId: 'string',
      criteria: {}
    };
    describe('#postThresholds - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postThresholds(thresholdsPostThresholdsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.objectInstanceId);
                assert.equal(true, Array.isArray(data.response.subObjectInstanceIds));
                assert.equal('object', typeof data.response.criteria);
                assert.equal('string', data.response.callbackUri);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'postThresholds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getThresholds - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getThresholds(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'getThresholds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getThresholdsThresholdId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getThresholdsThresholdId('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.objectInstanceId);
                assert.equal(true, Array.isArray(data.response.subObjectInstanceIds));
                assert.equal('object', typeof data.response.criteria);
                assert.equal('string', data.response.callbackUri);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'getThresholdsThresholdId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const thresholdsPatchThresholdsThresholdIdBodyParam = {
      callbackUri: 'string',
      authentication: {
        authType: [
          'TLS_CERT'
        ],
        paramsBasic: {
          userName: 'string',
          password: 'string'
        },
        paramsOauth2ClientCredentials: {
          clientId: 'string',
          clientPassword: 'string',
          tokenEndpoint: 'string'
        }
      }
    };
    describe('#patchThresholdsThresholdId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchThresholdsThresholdId('fakedata', thresholdsPatchThresholdsThresholdIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'patchThresholdsThresholdId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteThresholdsThresholdId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteThresholdsThresholdId('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'deleteThresholdsThresholdId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNfviCapacityInfos - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNfviCapacityInfos(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NfviCapacityInfos', 'getNfviCapacityInfos', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNfviCapacityInfosVimId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNfviCapacityInfosVimId('fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.vimId);
                assert.equal(true, Array.isArray(data.response.capacityInfoPerZone));
                assert.equal('object', typeof data.response.totalCapacityInfo);
                assert.equal('object', typeof data.response.timeInterval);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NfviCapacityInfos', 'getNfviCapacityInfosVimId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCapacityThresholds - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCapacityThresholds(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CapacityThresholds', 'getCapacityThresholds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const capacityThresholdsPostCapacityThresholdsBodyParam = {
      objectInstanceId: 'string',
      criteria: {}
    };
    describe('#postCapacityThresholds - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postCapacityThresholds(capacityThresholdsPostCapacityThresholdsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.objectInstanceId);
                assert.equal(true, Array.isArray(data.response.subObjectInstanceIds));
                assert.equal('object', typeof data.response.criteria);
                assert.equal('string', data.response.callbackUri);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CapacityThresholds', 'postCapacityThresholds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCapacityThresholdsCapacityThresholdId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCapacityThresholdsCapacityThresholdId('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.objectInstanceId);
                assert.equal(true, Array.isArray(data.response.subObjectInstanceIds));
                assert.equal('object', typeof data.response.criteria);
                assert.equal('string', data.response.callbackUri);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CapacityThresholds', 'getCapacityThresholdsCapacityThresholdId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const capacityThresholdsPatchCapacityThresholdsCapacityThresholdIdBodyParam = {
      callbackUri: 'string',
      authentication: {
        authType: [
          'OAUTH2_CLIENT_CREDENTIALS'
        ],
        paramsBasic: {
          userName: 'string',
          password: 'string'
        },
        paramsOauth2ClientCredentials: {
          clientId: 'string',
          clientPassword: 'string',
          tokenEndpoint: 'string'
        }
      }
    };
    describe('#patchCapacityThresholdsCapacityThresholdId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchCapacityThresholdsCapacityThresholdId('fakedata', capacityThresholdsPatchCapacityThresholdsCapacityThresholdIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CapacityThresholds', 'patchCapacityThresholdsCapacityThresholdId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCapacityThresholdsCapacityThresholdId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCapacityThresholdsCapacityThresholdId('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CapacityThresholds', 'deleteCapacityThresholdsCapacityThresholdId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vnfSnapshotPackagesPostVnfSnapshotPackagesBodyParam = {
      name: 'string'
    };
    describe('#postVnfSnapshotPackages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postVnfSnapshotPackages(vnfSnapshotPackagesPostVnfSnapshotPackagesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.vnfSnapshotPkgUniqueId);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.checksum);
                assert.equal('string', data.response.vnfSnapshotId);
                assert.equal(true, Array.isArray(data.response.vnfcSnapshotInfoIds));
                assert.equal(true, data.response.isFullSnapshot);
                assert.equal('object', typeof data.response.vnfdInfo);
                assert.equal('object', typeof data.response.vnfsr);
                assert.equal(true, Array.isArray(data.response.vnfcSnapshotImages));
                assert.equal(true, Array.isArray(data.response.additionalArtifacts));
                assert.equal('CREATED', data.response.state);
                assert.equal(true, data.response.isCancelPending);
                assert.equal('object', typeof data.response.failureDetails);
                assert.equal('object', typeof data.response.userDefinedData);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfSnapshotPackages', 'postVnfSnapshotPackages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVnfSnapshotPackages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVnfSnapshotPackages(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfSnapshotPackages', 'getVnfSnapshotPackages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVnfSnapshotPackagesVnfSnapshotPkgId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVnfSnapshotPackagesVnfSnapshotPkgId('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.vnfSnapshotPkgUniqueId);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.checksum);
                assert.equal('string', data.response.vnfSnapshotId);
                assert.equal(true, Array.isArray(data.response.vnfcSnapshotInfoIds));
                assert.equal(false, data.response.isFullSnapshot);
                assert.equal('object', typeof data.response.vnfdInfo);
                assert.equal('object', typeof data.response.vnfsr);
                assert.equal(true, Array.isArray(data.response.vnfcSnapshotImages));
                assert.equal(true, Array.isArray(data.response.additionalArtifacts));
                assert.equal('UPLOADING', data.response.state);
                assert.equal(true, data.response.isCancelPending);
                assert.equal('object', typeof data.response.failureDetails);
                assert.equal('object', typeof data.response.userDefinedData);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfSnapshotPackages', 'getVnfSnapshotPackagesVnfSnapshotPkgId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vnfSnapshotPackagesPatchVnfSnapshotPackagesVnfSnapshotPkgIdBodyParam = {
      name: 'string',
      userDefinedData: {},
      state: 'AVAILABLE'
    };
    describe('#patchVnfSnapshotPackagesVnfSnapshotPkgId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchVnfSnapshotPackagesVnfSnapshotPkgId('fakedata', vnfSnapshotPackagesPatchVnfSnapshotPackagesVnfSnapshotPkgIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfSnapshotPackages', 'patchVnfSnapshotPackagesVnfSnapshotPkgId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVnfSnapshotPackagesVnfSnapshotPkgId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVnfSnapshotPackagesVnfSnapshotPkgId('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfSnapshotPackages', 'deleteVnfSnapshotPackagesVnfSnapshotPkgId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVnfSnapshotPackagesVnfSnapshotPkgIdPackageContent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVnfSnapshotPackagesVnfSnapshotPkgIdPackageContent('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfSnapshotPackages', 'getVnfSnapshotPackagesVnfSnapshotPkgIdPackageContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVnfSnapshotPackagesVnfSnapshotPkgIdPackageContent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putVnfSnapshotPackagesVnfSnapshotPkgIdPackageContent('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfSnapshotPackages', 'putVnfSnapshotPackagesVnfSnapshotPkgIdPackageContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vnfSnapshotPackagesPostVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentUploadFromUriBodyParam = {
      addressInformation: 'string'
    };
    describe('#postVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentUploadFromUri - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentUploadFromUri('fakedata', vnfSnapshotPackagesPostVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentUploadFromUriBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfSnapshotPackages', 'postVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentUploadFromUri', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vnfSnapshotPackagesPostVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentBuildBodyParam = {
      vnfSnapshotInfoId: 'string'
    };
    describe('#postVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentBuild - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentBuild('fakedata', vnfSnapshotPackagesPostVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentBuildBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfSnapshotPackages', 'postVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentBuild', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vnfSnapshotPackagesPostVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentExtractBodyParam = {
      vnfSnapshotInfoId: 'string',
      vnfInstanceId: 'string'
    };
    describe('#postVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentExtract - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentExtract('fakedata', vnfSnapshotPackagesPostVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentExtractBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfSnapshotPackages', 'postVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentExtract', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vnfSnapshotPackagesPostVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentCancelBodyParam = {
      cause: 'string'
    };
    describe('#postVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentCancel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentCancel('fakedata', vnfSnapshotPackagesPostVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentCancelBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfSnapshotPackages', 'postVnfSnapshotPackagesVnfSnapshotPkgIdPackageContentCancel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVnfSnapshotPackagesVnfSnapshotPkgIdExtArtifactsAccess - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVnfSnapshotPackagesVnfSnapshotPkgIdExtArtifactsAccess('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfSnapshotPackages', 'getVnfSnapshotPackagesVnfSnapshotPkgIdExtArtifactsAccess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vnfSnapshotPackagesPutVnfSnapshotPackagesVnfSnapshotPkgIdExtArtifactsAccessBodyParam = {};
    describe('#putVnfSnapshotPackagesVnfSnapshotPkgIdExtArtifactsAccess - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putVnfSnapshotPackagesVnfSnapshotPkgIdExtArtifactsAccess('fakedata', vnfSnapshotPackagesPutVnfSnapshotPackagesVnfSnapshotPkgIdExtArtifactsAccessBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfSnapshotPackages', 'putVnfSnapshotPackagesVnfSnapshotPkgIdExtArtifactsAccess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVnfSnapshotPackagesVnfSnapshotPkgIdArtifactsArtifactPath - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVnfSnapshotPackagesVnfSnapshotPkgIdArtifactsArtifactPath('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfSnapshotPackages', 'getVnfSnapshotPackagesVnfSnapshotPkgIdArtifactsArtifactPath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#querymultipleNSinstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.querymultipleNSinstances(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsInstances', 'querymultipleNSinstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nsInstancesCreateaNSinstanceresourceBodyParam = {
      nsdId: 'string',
      nsName: 'string',
      nsDescription: 'string'
    };
    describe('#createaNSinstanceresource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createaNSinstanceresource(nsInstancesCreateaNSinstanceresourceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsInstances', 'createaNSinstanceresource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readanindividualNSinstanceresource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.readanindividualNSinstanceresource('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsInstances', 'readanindividualNSinstanceresource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNSinstanceresource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNSinstanceresource('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsInstances', 'deleteNSinstanceresource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nsInstancesInstantiateaNSBodyParam = {
      nsFlavourId: 'string'
    };
    describe('#instantiateaNS - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.instantiateaNS('fakedata', nsInstancesInstantiateaNSBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsInstances', 'instantiateaNS', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nsInstancesScaleaNSinstanceBodyParam = {};
    describe('#scaleaNSinstance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.scaleaNSinstance('fakedata', nsInstancesScaleaNSinstanceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsInstances', 'scaleaNSinstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nsInstancesUpdatesaNSinstanceBodyParam = {
      updateType: 'ADD_VNF'
    };
    describe('#updatesaNSinstance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatesaNSinstance('fakedata', nsInstancesUpdatesaNSinstanceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsInstances', 'updatesaNSinstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nsInstancesHealaNSinstanceBodyParam = {};
    describe('#healaNSinstance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.healaNSinstance('fakedata', nsInstancesHealaNSinstanceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsInstances', 'healaNSinstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nsInstancesTerminateaNSinstanceBodyParam = {
      terminationTime: null
    };
    describe('#terminateaNSinstance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.terminateaNSinstance('fakedata', nsInstancesTerminateaNSinstanceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsInstances', 'terminateaNSinstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#querymultipleNSLCMoperationoccurrences - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.querymultipleNSLCMoperationoccurrences(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsLcmOpOccs', 'querymultipleNSLCMoperationoccurrences', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readanindividualNSLCMoperationoccurrenceresource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.readanindividualNSLCMoperationoccurrenceresource('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsLcmOpOccs', 'readanindividualNSLCMoperationoccurrenceresource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retryaNSlifecyclemanagementoperationoccurrence - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retryaNSlifecyclemanagementoperationoccurrence('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsLcmOpOccs', 'retryaNSlifecyclemanagementoperationoccurrence', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rollbackaNSlifecyclemanagementoperationoccurrence - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rollbackaNSlifecyclemanagementoperationoccurrence('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsLcmOpOccs', 'rollbackaNSlifecyclemanagementoperationoccurrence', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#continueaNSlifecyclemanagementoperationoccurrence - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.continueaNSlifecyclemanagementoperationoccurrence('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NsLcmOpOccs', 'continueaNSlifecyclemanagementoperationoccurrence', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#markaNSlifecyclemanagementoperationoccurrenceasfailed - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.markaNSlifecyclemanagementoperationoccurrenceasfailed('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nslcm', 'markaNSlifecyclemanagementoperationoccurrenceasfailed', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nslcmCancelaNSlifecyclemanagementoperationoccurrenceBodyParam = {
      cancelMode: 'GRACEFUL'
    };
    describe('#cancelaNSlifecyclemanagementoperationoccurrence - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancelaNSlifecyclemanagementoperationoccurrence('fakedata', nslcmCancelaNSlifecyclemanagementoperationoccurrenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nslcm', 'cancelaNSlifecyclemanagementoperationoccurrence', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#queryVNFsnapshots - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.queryVNFsnapshots(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfSnapshots', 'queryVNFsnapshots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#queryanIndividualVNFsnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.queryanIndividualVNFsnapshot('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfSnapshots', 'queryanIndividualVNFsnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteanIndividualVNFsnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteanIndividualVNFsnapshot('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-etsi_sol005-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VnfSnapshots', 'deleteanIndividualVNFsnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
