
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:10PM

See merge request itentialopensource/adapters/adapter-etsi_sol005!13

---

## 0.4.3 [08-25-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-etsi_sol005!11

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:19PM

See merge request itentialopensource/adapters/adapter-etsi_sol005!10

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:32PM

See merge request itentialopensource/adapters/adapter-etsi_sol005!9

---

## 0.4.0 [07-08-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-etsi_sol005!8

---

## 0.3.4 [03-28-2024]

* Changes made at 2024.03.28_13:14PM

See merge request itentialopensource/adapters/cloud/adapter-etsi_sol005!7

---

## 0.3.3 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/cloud/adapter-etsi_sol005!6

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_15:31PM

See merge request itentialopensource/adapters/cloud/adapter-etsi_sol005!5

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:42AM

See merge request itentialopensource/adapters/cloud/adapter-etsi_sol005!4

---

## 0.3.0 [12-27-2023]

* Adapter Migration

See merge request itentialopensource/adapters/cloud/adapter-etsi_sol005!3

---

## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-etsi_sol005!1

---

## 0.1.1 [03-02-2022]

- Initial Commit

See commit 34782c1

---
