
## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-etsi_sol005!1

---

## 0.1.1 [03-02-2022]

- Initial Commit

See commit 34782c1

---
