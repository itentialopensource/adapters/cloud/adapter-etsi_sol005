# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Etsi_sol005 System. The API that was used to build the adapter for Etsi_sol005 is usually available in the report directory of this adapter. The adapter utilizes the Etsi_sol005 API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The ETSI NFV-SOL 005 adapter from Itential is used to integrate the Itential Automation Platform (IAP) with ETSI NFV-SOL 005 to facilitate automation and resource optimization. 

With this adapter you have the ability to perform operations with ETSI NFV-SOL 005 such as:
- Alarms
- Capacity Thresholds
- Subscriptions
- VNF

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
